# Statistique mensuelle 
<!-- SPDX-License-Identifier: MPL-2.0 -->

Statistiques de dépenses par type de risque, publiées chaque mois par l'assurance maladie. 


## Références 

- [Page](https://www.ameli.fr/l-assurance-maladie/statistiques-et-publications/donnees-statistiques/depenses-d-assurance-maladie/depenses-par-type-de-risque/depenses-mensuelles-2019.php) de présentation et de publication sur le site de l'assurance maladie.
